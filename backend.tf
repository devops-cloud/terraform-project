terraform {
  backend "s3" {
    bucket = "terraform-backend-state-keeper"
    key    = "states"
    region = "us-east-1"
  }
}