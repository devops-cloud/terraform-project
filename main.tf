# Configure the AWS Provider
provider "aws" {
//  access_key = "${var.aws_access_key}"
//  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}


data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}


resource "aws_instance" "master_instance" {

  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.deployer.key_name}"
  #security_groups = ["${aws_security_group.allow_tls.id}"]
  vpc_security_group_ids=["${aws_security_group.allow_tls.id}"]
  subnet_id = "${aws_subnet.subnet.id}"

  tags = {
    Name = "master_instance"
  }

  connection {
    type = "ssh"
    user = "ubuntu"
//    private_key="${file("~/.ssh/aws_rsa")}"
    private_key="${var.aws_instance_key}"
    agent = false
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt install -y apt-transport-https ca-certificates curl software-properties-common",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
      "sudo add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable edge\"",
      "apt-cache policy docker-ce",
      "sudo apt-get update",
      "sudo apt-get install -y docker-ce",
      "sudo docker swarm init --advertise-addr ${aws_instance.master_instance.private_ip}",
      "sudo docker swarm join-token --quiet worker > /home/ubuntu/token"
    ]
  }
}

resource "aws_instance" "slave_instance" {

  count = 3
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.deployer.key_name}"
  #security_groups = ["${aws_security_group.allow_tls.id}"]
  vpc_security_group_ids=["${aws_security_group.allow_tls.id}"]
  subnet_id = "${aws_subnet.subnet.id}"

  tags = {
    Name = "slave_instance${count.index}"
  }

  connection {
    type = "ssh"
    user = "ubuntu"
    #private_key="${file("~/.ssh/aws_rsa")}"
    private_key="${var.aws_instance_key}"
    agent = false
    timeout = "10m"
  }

  provisioner "file" {
    #source = "~/.ssh/aws_rsa"
    content = "${var.aws_instance_key}"
    destination = "/home/ubuntu/key"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt install -y apt-transport-https ca-certificates curl software-properties-common",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
      "sudo add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable edge\"",
      "apt-cache policy docker-ce",
      "sudo apt-get update",
      "sudo apt-get install -y docker-ce",
      "sudo chmod 400 /home/ubuntu/key",
      "sudo scp -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null -i /home/ubuntu/key ubuntu@${aws_instance.master_instance.private_ip}:/home/ubuntu/token /home/ubuntu/token",
      "sudo docker swarm join --token $(cat /home/ubuntu/token) ${aws_instance.master_instance.private_ip}:2377"
    ]
  }
}