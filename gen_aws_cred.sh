#!/usr/bin/env bash

cat <<EOF > ~/.aws/credentials
[default]
aws_access_key_id = ${TF_VAR_aws_access_key}
aws_secret_access_key = ${TF_VAR_aws_secret_key}
EOF