resource "aws_eip" "elastic_ip_master" {

  instance = "${aws_instance.master_instance.id}"
  vpc      = true
  depends_on                = ["aws_instance.master_instance", "aws_internet_gateway.gw"]
}

resource "aws_eip" "elastic_ip_slave0" {

  instance = "${aws_instance.slave_instance.0.id}"
  vpc      = true
  depends_on                = ["aws_instance.slave_instance", "aws_internet_gateway.gw"]
}

resource "aws_eip" "elastic_ip_slave1" {

  instance = "${aws_instance.slave_instance.1.id}"
  vpc      = true
  depends_on                = ["aws_instance.slave_instance", "aws_internet_gateway.gw"]
}

resource "aws_eip" "elastic_ip_slave2" {

  instance = "${aws_instance.slave_instance.2.id}"
  vpc      = true
  depends_on                = ["aws_instance.slave_instance", "aws_internet_gateway.gw"]
}